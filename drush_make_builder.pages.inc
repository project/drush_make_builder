<?php

/**
 * Main admin form.
 */
function drush_make_builder_admin() {
  $form = array();

  // Check for existing make files.
  $count = db_result(db_query("SELECT COUNT(*) FROM {drush_make_builder}"));
  // Check for existing hosted make files.
  $hosted_count = db_result(db_query("SELECT COUNT(*) FROM {drush_make_builder} WHERE host = 1"));
  // If we have make files, collapse the New Make File dialogue.
  $collapsed = FALSE;
  if ($count) {
    $collapsed = TRUE;
  }
  
  // Check for results.
  if ($count) {
    // So we only need query once, build the data we need here.
    $result = db_query("SELECT * FROM {drush_make_builder} ORDER BY name");
    $rows = array();
    $options = array();
    $hosted_options = array();
    while ($row = db_fetch_array($result)) {
      // Table data first.
      $rows[$row['mfid']] = array(
        $row['host'] ? l($row['name'] . '.make', 'drush_make/' . $row['name'] . '.make') : l($row['name'] . '.make', 'admin/build/drush_make/' . $row['name'] . '.make'),
        $row['host'] ? t('Yes') : t('No'),
        l(t('edit'), 'admin/build/drush_make/edit/' . $row['mfid']),
        l(t('delete'), 'admin/build/drush_make/delete/' . $row['mfid']),
      );
      // Now build the checkbox options to select from existing make files (if any).
      // We can only include hosted make files.
      if ($row['host'] && $hosted_count) {
        $hosted_options[$row['mfid']] = $row['name'] . '.make';
      }
      // Finally aggregated make files.
      $options[$row['mfid']] = $row['name'] . '.make';
    }
  }

  // Make sure we're allowed to create new make files.
  if (user_access('create make files')) {
    $form['drush_make_new_file'] = array(
      '#description' => t('Create a new Drush Make file.'),
      '#title' => t('New Drush Make file'),
      '#type' => 'fieldset',
      '#collapsed' => $collapsed,
      '#collapsible' => TRUE,
    );
    $form['drush_make_new_file']['drush_make_new_file_name'] = array(
      '#description' => t('Enter the name of your make file. This value must be unique and machine readable. The .make file extension will be automatically added.'),
      '#title' => t('File name'),
      '#type' => 'textfield',
      '#required' => TRUE,
    );
    if ($hosted_count) {
      // Place in fieldset for better UI.
      $form['drush_make_new_file']['text_box'] = array(
        '#description' => t('Create a new Drush Make file by entering the contents of your make file in the box below.'),
        '#title' => t('New Drush Make file as text'),
        '#type' => 'fieldset',
        '#collapsed' => TRUE,
        '#collapsible' => TRUE,
      );
      $form['drush_make_new_file']['text_box']['drush_make_new_file_contents'] = array(
        '#description' => t('Leave blank if you intend to pick from existing make files.'),
        '#title' => t('File contents'),
        '#type' => 'textarea',
      );
    }
    else {
      $form['drush_make_new_file']['drush_make_new_file_contents'] = array(
        '#description' => t('Enter the contents of your make file in this box. Leave blank if you intend to pick from existing make files.'),
        '#title' => t('File contents'),
        '#type' => 'textarea',
      );
    }
  
    // Check there are hosted make files.
    if ($hosted_count) {
      // Place in fieldset for better UI.
      $form['drush_make_new_file']['file_picker_include'] = array(
        '#description' => t('Create a new Drush Make file from existing hosted make files using include. IMPORTANT: drush must be able to access the make files if you use this approach (e.g. the \'anonymous user\' role must have the \'access make files\' permission).'),
        '#title' => t('New Drush Make file from existing using includes'),
        '#type' => 'fieldset',
        '#collapsed' => FALSE,
        '#collapsible' => TRUE,
      );
      // Build the form elements.
      $form['drush_make_new_file']['file_picker_include']['drush_make_new_file_picker'] = array(
        '#description' => t('Select the make files you would like to include for your new make file.'),
        '#title' => t('Include selected make files'),
        '#type' => 'checkboxes',
        '#options' => $hosted_options,
      );
    }
    
    // This is for aggregated files, so no need to check for hosted.
    if ($count) {
      // Place in fieldset for better UI.
      $form['drush_make_new_file']['file_picker_aggregate'] = array(
        '#description' => t('Create a new aggregated Drush Make file from existing make files.'),
        '#title' => t('New Drush Make file from existing by aggregation'),
        '#type' => 'fieldset',
        '#collapsed' => FALSE,
        '#collapsible' => TRUE,
      );
      // Build the form elements.
      $form['drush_make_new_file']['file_picker_aggregate']['drush_make_new_file_picker_aggregate'] = array(
        '#description' => t('Select the make files you would like to aggregate for your new make file.'),
        '#title' => t('Aggregate selected make files'),
        '#type' => 'checkboxes',
        '#options' => $options,
      );
    }
    
    if ($count || $hosted_count) {
      $form['drush_make_new_file']['drush_make_new_file_include_core'] = array(
        '#description' => t('Check this box to include core in your make file. This is ignored if a make file is manually created using the textarea.'),
        '#title' => t('Include Drupal core'),
        '#type' => 'checkbox',
      );
    }
    $form['drush_make_new_file']['drush_make_new_file_host_file'] = array(
      '#description' => t('Check this box to have Drupal host this make file to users with the \'access make files\' permission.'),
      '#title' => t('Host this make file'),
      '#type' => 'checkbox',
    );
  }
  
  // There are stored make files.
  if ($count) {
    // Set the table headers.
    $header = array(
      array(
        'data' => 'Name',
      ),
      array(
        'data' => 'Hosted',
      ),
      array(
        'data' => 'Edit',
      ),
      array(
        'data' => 'Delete',
      ),
    );
    $form['drush_make_files'] = array(
      '#type' => 'markup',
      '#value' => theme('table', $header, $rows),
    );
  }
  // There are no stored make files.
  else {
    $form['drush_make_no_files'] = array(
      '#type' => 'item',
      '#value' => 'No make files available.',
    );
  }

  $form['drush_make_file_submit'] = array(
    '#type' => 'submit',
    '#default_value' => t('Create new make file'),
  );

  return $form;
}

/**
 * Implementation of hook_validate() for the admin form.
 */
function drush_make_builder_admin_validate($form, &$form_state) {
  $selected = FALSE;
  $files = $form_state['values']['drush_make_new_file_picker'];
  if ($files) {  
    foreach ($files as $use) {
      if ($use) {
        $selected = TRUE;
      }
    }
  }
  $files = $form_state['values']['drush_make_new_file_picker_aggregate'];
  foreach ($files as $use) {
    if ($use) {
      $selected = TRUE;
    }
  }
  if (!$form_state['values']['drush_make_new_file_contents'] && !$selected) {
    form_set_error('drush_make_new_file_contents', t('You must either provide text for your make file or select existing make files to include or aggregate.'));
  }
  if (preg_match('/[^a-z0-9-]/', $form_state['values']['drush_make_new_file_name'])) {
    form_set_error('drush_make_new_file_name', t('The make file name may only consist of lowercase letters, numbers, and hyphens.'));
  }
}

/**
 * Implementation of hook_submit() for the admin form.
 */
function drush_make_builder_admin_submit($form, &$form_state) {
  // Saving a new make file based on textarea contents.
  if ($form_state['values']['drush_make_new_file_contents']) {
    $sql = "INSERT INTO {drush_make_builder} (content, host, name) VALUES ('%s', %d, '%s')";
    db_query($sql, serialize($form_state['values']['drush_make_new_file_contents']), $form_state['values']['drush_make_new_file_host_file'], $form_state['values']['drush_make_new_file_name']);
  }
  // Saving a new make file compiled from hosted make files.
  else {
    // Detect server base URL.
    $proto = $_SERVER['HTTPS'] ? 'https://' : 'http://';
    $host = $_SERVER['SERVER_NAME'];
    $port = ($_SERVER['SERVER_PORT'] == 80 ? '' : ':'. $_SERVER['SERVER_PORT']);
    $uri = $proto . $host . $port . '/drush_make/';
    $output = "; This make file was generated by the Drush Make Builder module.\n";
    $output .= "; If you are using includes, you must have access over HTTP to make files hosted at $proto$host$port from the command line for this file to work.\n";
    $output .= "; If you only use aggregation then access does not matter.\n\n";

    if ($form_state['values']['drush_make_new_file_include_core']) {
      $output .= "core = 6.x\n\n";
    }
    // Check for included files and write them to the new make file.
    $files = $form_state['values']['drush_make_new_file_picker'];
    if ($files) {
      foreach ($files as $mfid => $use) {
        if ($use) {        
          // Create an include for each selected make file.
          $sql = "SELECT name FROM {drush_make_builder} WHERE mfid = %d";
          $name = db_result(db_query($sql, $mfid));
          $output .= "includes[$name] = \"$uri$name.make\"\n";
        }
      }
    }
    // Check for aggregated files and write them to the new make file.
    $files = $form_state['values']['drush_make_new_file_picker_aggregate'];
    foreach ($files as $mfid => $use) {
      if ($use) {
        // Aggregate each selected make file.
        $sql = "SELECT content FROM {drush_make_builder} WHERE mfid = %d";
        $file = unserialize(db_result(db_query($sql, $mfid)));
        $output .= "$file\n\n";
      }
    }
    $sql = "INSERT INTO {drush_make_builder} (content, host, name) VALUES ('%s', %d, '%s')";
    db_query($sql, serialize($output), $form_state['values']['drush_make_new_file_host_file'], $form_state['values']['drush_make_new_file_name']);
  }
  // Rebuild the menus so our hook_menu() is invoked.
  menu_rebuild();
  drupal_set_message(t('Make file saved.'));
}

/**
 * Returns the raw text data stored in the database.
 * 
 * @param $mfid
 *    Integer, make file ID of the target make file.
 * 
 * @return
 *    Raw text string of Drush Make file.
 */
function drush_make_builder_get_file($mfid) {
  $result = db_query("SELECT * FROM {drush_make_builder} WHERE mfid = %d", $mfid);
  $file = db_fetch_array($result);

  // set headers
  drupal_set_header('Content-Type: text/plain; charset=utf-8');
  drupal_set_header('Content-Disposition: attachment; filename="' . $file['name'] .'.make"');

  return unserialize($file['content']);
}

/**
 * Returns the raw text data stored in the database in the context
 * of a form textarea for copy and paste.
 * 
 * @param $mfid
 *    Integer, make file ID of the target make file.
 * 
 * @return
 *    Raw text string of Drush Make file.
 */
function drush_make_builder_show_file($mfid) {
  $result = db_query("SELECT * FROM {drush_make_builder} WHERE mfid = %d", $mfid);
  $file = db_fetch_array($result);
  return drupal_get_form('drush_make_builder_show_file_form', unserialize($file['content']));
}

/**
 * Form for copying and pasting make files.
 */
function drush_make_builder_show_file_form($form_id, $content) {
  $form = array();
  $form['drush_make_show_file'] = array(
    '#type' => 'textarea',
    '#title' => t('Make file contents'),
    '#description' => t('Copy and paste this to your local .make file for use.'),
    '#default_value' => $content,
    '#rows' => 20,
  );
  $form['drush_make_builder_cancel'] = array(
    '#type' => 'markup',
    '#value' => l(t('Back'), 'admin/build/drush_make'),
  );
  return $form;
}

/**
 * Make file delete form.
 * 
 * @param $mfid
 *    Integer, make file ID of the target make file.
 */
function drush_make_builder_admin_delete($form_id, $mfid) {
  $form = array();
  $form['drush_make_delete_mfid'] = array(
    '#type' => 'value',
    '#value' => $mfid,
  );
  $name = db_result(db_query("SELECT name FROM {drush_make_builder} WHERE mfid = %d", $mfid));
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $name . '.make')),
    'admin/build/drush_make',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Implementation of hook_submit() for the make file delete form.
 */
function drush_make_builder_admin_delete_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $sql = "DELETE FROM {drush_make_builder} WHERE mfid = %d";
    db_query($sql, $form_state['values']['drush_make_delete_mfid']);
  }
  $form_state['redirect'] = 'admin/build/drush_make';
}

/**
 * Make file edit form.
 * 
 * @param $mfid
 *    Integer, make file ID of the target make file.
 */
function drush_make_builder_admin_edit($form_id, $mfid) {
  $form = array();
  $sql = "SELECT * FROM {drush_make_builder} WHERE mfid = %d";
  $file = db_fetch_array(db_query($sql, $mfid));
  
  $form['drush_make_builder_edit_mfid'] = array(
    '#type' => 'value',
    '#value' => $mfid,
  );
  $form['drush_make_builder_edit_name'] = array(
    '#description' => t('Alter the name of your make file. This value must be unique and machine readable. The .make file extension will be automatically added.'),
    '#title' => t('File name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $file['name'],
  );
  $form['drush_make_builder_edit_content'] = array(
    '#description' => t('Alter the contents of your make file.'),
    '#title' => t('File contents'),
    '#type' => 'textarea',
    '#required' => TRUE,
    '#default_value' => unserialize($file['content']),
    '#rows' => 10,
  );
  $form['drush_make_builder_edit_host'] = array(
    '#description' => t('Check this box to have Drupal host this make file to users with the \'access make files\' permission.'),
    '#title' => t('Host this make file'),
    '#type' => 'checkbox',
    '#default_value' => $file['host'],
  );
  $form['drush_make_builder_submit'] = array(
    '#type' => 'submit',
    '#default_value' => t('Save'),
  );
  $form['drush_make_builder_cancel'] = array(
    '#type' => 'markup',
    '#value' => l(t('Cancel'), 'admin/build/drush_make'),
  );
  
  $form['#redirect'] = 'admin/build/drush_make';
  
  return $form;
}

/**
 * Implementation of hook_validate() for the make file edit form.
 */
function drush_make_builder_admin_edit_validate($form, &$form_state) {
  if (preg_match('/[^a-z0-9-]/', $form_state['values']['drush_make_builder_edit_name'])) {
    form_set_error('drush_make_builder_edit_name', t('The make file name may only consist of lowercase letters, numbers, and hyphens.'));
  }
}

/**
 * Implementation of hook_submit() for the make file edit form.
 */
function drush_make_builder_admin_edit_submit($form, &$form_state) {
  $sql = "UPDATE {drush_make_builder} SET content='%s', host=%d, name='%s' WHERE mfid = %d";
  db_query($sql, serialize($form_state['values']['drush_make_builder_edit_content']), $form_state['values']['drush_make_builder_edit_host'], $form_state['values']['drush_make_builder_edit_name'], $form_state['values']['drush_make_builder_edit_mfid']);
  // Rebuild the menus so our hook_menu() is invoked.
  menu_rebuild();
  drupal_set_message(t('Your changes have been saved.'));
}
