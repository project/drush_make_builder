DRUSH MAKE BUILDER
==================

Written by Greg Harvey.
http://www.drupaler.co.uk / http://www.cmspros.co.uk


WHY DOES THIS MODULE EXIST?
===========================

I thought wouldn't it be cool if I had a load of make files for granular tasks,
such as WYSIWYG set-up, image gallery, commenting, all as separate make files,
and I could quickly and easily compile a 'master' make file by selecting the
bits I want for a particular site. Then I thought it would be cooler still if I
could host those files in a Drupal site and share them, publicly or with a 
distributed team on an intranet. 

So I made this module.


HOW DO I USE IT?
================

Install and enable in the usual way. You'll find a new 'Drush Make' menu item
under 'Site Building'. There are also three new permissions, 'access make
files', 'administer make files' and 'create make files'. If you are not user 1
assign yourself the appropriate permissions before continuing.

Assuming you have 'create' permissions, when you first get there you'll only 
have a few fields:

  File name => The machine-readable name of your make file.
  File contents => The content of your new make file. (Sorry, this is manual 
    for now.)
  Host this make file => Checkbox to determine if the make file should be
    hosted as a downloadable resource or just available to permissioned 
    admin users.

Once you have created your first make file then the form gets more 
interesting. Your make file will be listed in a table under the collapsed
'New Drush Make file' fieldset. Clicking on the name will either start the
download (if you ticked 'Host this make file') or take you to a page
presenting the make file in a textarea so you can copy and paste it. The 
edit and delete links are self-explanatory!

If you expand the 'New Drush Make file' fieldset you will find the initial
'File contents' box is collapsed and (depending on whether you opted to 
host the make file you created) two new interface elements have appeared:

  Include selected make files => Only works with 'hosted' make files,
    actually writes a make file with an 'include' entry for each selected
    existing file, instead of aggregating the make file wholesale in to a new 
    make file. Note the caveats about HTTP access for this to work.
  Aggregate selected make files => Can be used with all make files, 
    hosted or otherwise, and each selected existing file is aggregated in to a
    new 'master' make file which is, in turn, saved and may be 'hosted' if you
    so wish.
    
Note, you can mix and match these options (so you could include a couple of 
files then aggregate another three). Why you'd want to, I'm not sure, but the
flexibility may be useful to some people. There is also a new 'Include Drupal
core' checkbox at the bottom of the form, which simply adds the all-important
'core = 6.x' line to the beginning of your make file, should you require it.

Note, you can also make quite horribly broken make files with this tool if you
don't know what you're doing. This doesn't matter per se, the result will be a
make file you can't use, so it's not the end of the world. But take care which
files are are aggregating/including and make sure they don't trip over one
another, include core twice, etc. etc.


WHAT IS THE FUTURE?
===================

I'd like web services integration (with the Services module) so make files can
be shared across web services. I'd also like to be able to build a new make
file by ticking boxes against available packages in a given Drupal installation
(including 3rd party libraries) but I suspect that's quite involved, so parked
for now. Some clever validation of make files on save would be good too, to
prevent the saving of broken make files as described above.
